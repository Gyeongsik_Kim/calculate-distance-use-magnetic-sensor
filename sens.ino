#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <LiquidCrystal.h>

/**
* LCD PIN
* LCD RS -> A5
* LCD Enable -> A4
* LCD D4 -> A3
* LCD D5 -> A2
* LCD D6 -> A1
* LCD D7 -> A0
* LCD R/W -> GND
**/

//디버깅 상수
//define을 제거하면 디버깅용 정보가 출력되지 않음
#define DEBUG

//핀 설정
#define BUZZER_PIN A15
const int led[] = {22,23,24};

//센서 인스턴스
Adafruit_LSM303_Mag_Unified mag = Adafruit_LSM303_Mag_Unified(12345);
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);

//LCD 인스턴스
//LiquidCrystal(rs, enable, d4, d5, d6, d7)
LiquidCrystal lcd(A5, A4, A3, A2, A1, A0);

//차이나는 벡터별 길이 테이블
const float lengthTable[] = {11, 9, 8, 7.5, 7, 5.3, 4.8, 4.5, 4, 3.5, 3.1, 2.9, 2.7, 2.5, 2.4, 2.2, 2, 1.8, 1.7, 1.6, 1.5};

//벡터값 구조체
typedef struct {
  float x, y, z;
} Vector;

//기준 자기장과 가속도 값 저장 변수, 0으로 초기화
Vector defaultV = {0,0,0}, defaultA = {0,0,0};

//초기 설정을 해주는 함수
void setup(void)
{
#ifndef ESP8266
  while (!Serial);
#endif
  Serial.begin(9600);

  /* Enable auto-gain */
  mag.enableAutoRange(true);
  pinMode(BUZZER_PIN, OUTPUT);
  for(int i = 0;i<3;i++){
    pinMode(led[i], OUTPUT);
  }
  /* Initialise the sensor */
  if(!mag.begin())
  {
    /* There was a problem detecting the LSM303 ... check your connections */
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while(1);
  } /* Initialise the sensor */
  if(!accel.begin())
  {
    /* There was a problem detecting the ADXL345 ... check your connections */
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while(1);
  }
  lcd.begin(16, 2);

}

//새로 들어온 자기장 벡터와 초기 실행시 읽은 자기장 벡터와
//크기의 차이로 길이를 계산하는 함수
//value는 초기 자기장 벡터와 새로 읽은 벡터와의 차이 값
int getLength(float value){
  if(lengthTable[0] < value)
    return 19;
  for(int i = 1;i<sizeof(lengthTable) / sizeof(lengthTable[1]);i++){
    if(lengthTable[i - 1] > value && lengthTable[i] < value){
      return i + 20;
    }
  }
  return 50;
}

//LCD에 거리를 출력해주는 함수
//mag는 자기장으로 계산한 거리
//ir은 적외선 센서로 계산된 거리
void display_distance(float mag, float ir){
  lcd.clear();
  char* text = new char[16];
  sprintf(text, "IR : %.1f, MAG : %.1f", ir, mag);
  lcd.setCursor(0, 0);
  lcd.print(text);
  delete text;
}

//LOOP!
void loop(void){
  bool cali = false, next = true;
  sensors_event_t event;
  mag.getEvent(&event);

  //기준 자기장값과 비교해서 차이나는 벡터값 계산
  float dif = sqrt(sq(event.magnetic.x - defaultV.x) + sq(event.magnetic.y - defaultV.y) + sq(event.magnetic.z - defaultV.z));

  //현재 자기장 값 임시 저장 변수
  float tempX, tempY, tempZ;

  //기준 자기장의 값이 0이면 처음 켜진 직후 상태, 현재의 값으로 초기화 해준다.
  if(defaultV.x == 0 && defaultV.y == 0 && defaultV.z == 0 ){
    cali = true;
    defaultV.x = event.magnetic.x;
    defaultV.y = event.magnetic.y;
    defaultV.z = event.magnetic.z;
  }
  tempX = event.magnetic.x;
  tempY = event.magnetic.y;
  tempZ = event.magnetic.z;
  #ifdef DEBUG
    Serial.print("X: "); Serial.print(event.magnetic.x); Serial.print("  ");
    Serial.print("Y: "); Serial.print(event.magnetic.y); Serial.print("  ");
    Serial.print("Z: "); Serial.print(event.magnetic.z); Serial.print("  ");

    Serial.print("X: "); Serial.print(event.magnetic.x - defaultV.x); Serial.print("  ");
    Serial.print("Y: "); Serial.print(event.magnetic.y - defaultV.y); Serial.print("  ");
    Serial.print("Z: "); Serial.print(event.magnetic.z - defaultV.z); Serial.print("  ");

    Serial.print(dif); Serial.print("  ");Serial.println("uT");
  #endif

  //값이 튀는 경우의 예외처리
  if(sqrt(sq(event.magnetic.x)+sq(event.magnetic.y)+sq(event.magnetic.z)) < 4){
    return;
  }

  accel.getEvent(&event);
  /* Display the results (speed is measured in rad/s) */
  #ifdef DEBUG
    Serial.print("X: "); Serial.print(event.acceleration.x); Serial.print("  ");
    Serial.print("Y: "); Serial.print(event.acceleration.y); Serial.print("  ");
    Serial.print("Z: "); Serial.print(event.acceleration.z); Serial.print("  ");
  #endif
  //기준 가속도의 값이 0이면 처음 켜진 상태이므로 현재의 값으로 초기화
  if(defaultA.x == 0 && defaultA.y == 0 && defaultA.z == 0){
    cali = true;
    defaultA.x = event.acceleration.x;
    defaultA.y = event.acceleration.y;
    defaultA.z = event.acceleration.z;
  }
  //기준 가속도 값과 비교해서 차이나는 벡터를 계산
  float difA = sqrt(sq(event.acceleration.x - defaultA.x) + sq(event.acceleration.y - defaultA.y) + sq(event.acceleration.z - defaultA.z));

  #ifdef DEBUG
    Serial.println(difA);
  #endif

  //차이나는 가속도 벡터가 1 미만이고, 차이나는 자기장 벡터가 0.5 이하면
  //가만히 있는데 값이 떨리는 상태이므로, 기준값을 현재 값으로 변경
  if(difA < 1 && dif < 0.5){
    cali = true;
    defaultV.x = tempX;
    defaultV.y = tempY;
    defaultV.z = tempZ;
  }else if(difA > 1){
    //차이나는 가속도 벡터가 1 초과면
    //센서가 움직이는 상태이므로, 새로 값을 받아와서 기준값을 변경
    cali = true;
    mag.getEvent(&event);
    defaultV.x  = event.magnetic.x;
    defaultV.y  = event.magnetic.y;
    defaultV.z  = event.magnetic.z;
    accel.getEvent(&event);
    defaultA.x = event.acceleration.x;
    defaultA.y = event.acceleration.y;
    defaultA.z = event.acceleration.z;
  }

  //이전에 HIGH로 설정되거나 출력중인 핀들 정리
  for(int i = 0;i<3;i++){
    digitalWrite(led[i], HIGH);
  }
  noTone(BUZZER_PIN);

  //오차 범위 내인지 아닌지 체크
  if(!cali){
    //기준값과 현재의 자기장 값과 비교하여 차이 계산
    dif = sqrt(sq(tempX - defaultV.x) + sq(tempY - defaultV.y) + sq(tempZ - defaultV.z));
    float range = (float)getLength(dif) / 10.0;
    if(range == 5.0){
      return;
    }
    Serial.print(range); Serial.println("cm");
    int temp = (int)(range - 0.5);
    if(range < 2){
      //~2cm
      next = false;
      Serial.print("MIN!");
      digitalWrite(led[0], LOW);
      tone(BUZZER_PIN, 1760);
      delay(1000);
    }else if(range >= 2 & range < 3){
      //2~3cm
      next = false;
      digitalWrite(led[0], LOW);
      for(int i = 0;i<4;i++){
        tone(BUZZER_PIN, 1760);
        delay(30);
        noTone(BUZZER_PIN);
        delay(220);
      }
    }else if(range >= 3 & range < 4){
      next = false;
      //3~4cm
       digitalWrite(led[1], LOW);
      for(int i = 0;i<2;i++){
        tone(BUZZER_PIN, 1760);
        delay(30);
        noTone(BUZZER_PIN);
        delay(470);
      }
    }else if(range >= 4 & range < 4.5){
      next = false;
      //4~5cm
       digitalWrite(led[2], LOW);
      tone(BUZZER_PIN, 1760);
      delay(30);
      noTone(BUZZER_PIN);
      delay(970);
    }
  }else{
    Serial.println("CALI!");
  }

  //초기 벡터 리셋용 명령어 'r'
  if(Serial.available()){
    switch(Serial.read()){
      case 'r' :{
         defaultV.x = defaultV.y = defaultV.z = 0;
        break;
      }
    }
  }


  if(next)
    delay(1000);

}
